PS C:\Users\Context\Desktop\activemq> java -jar .\activeMQDump-0.0.1-SNAPSHOT-jar-with-dependencies.jar
usage: java -jar ActiveMQDump.jar
 -i,--ipaddress <arg>   IP address of target ActiveMQ server.
 -k                     Ignore invalid SSL/TLS certificates
 -m                     Enumerate messages
 -o,--output <arg>      Path where to dump retrieved messages
 -p,--port <arg>        Port of target ActiveMQ server. (Default 61616)
    --pass <arg>        Specify password
 -q                     Enumerate queues
 -s                     Enable TLS. Default is unencrypted.
 -t                     Enumerate topics
    --user <arg>        Specify username

#####

java -jar activeMQDump-0.0.1-SNAPSHOT-jar-with-dependencies.jar -i <ip> -p 4444 -q -m -o /tmp/
"-q" instructs the tool to retreive all queue names and
"-m" makes sure to retrieve all messages that are stored in the previously identified queues.
With "-o" we specified a path where to store the dumped messages.
