```
[1] grep -r "Chain of trust" *.log | grep "NOT ok" | grep "self" | cut  -d '-' -f1 | sed -e 's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[2] grep -r "Certificate Expiration" *.log | grep "expired" | cut  -d '-' -f1 | sed -e 's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[3] grep -r "Common Name" *.log | awk '{print $5,$1}' | egrep "\*" | cut  -d '-' -f1 | sed -e 's/_p/:/g' -e "s/$/\/tcp/g" | uniq | awk '{print $2,$1}'
[4] grep -r "Signature Algorithm" *.log | grep "SHA1" | cut  -d '-' -f1 | sed -e 's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[5] grep -r "Server key size" *.log  | grep "RSA" | grep -E '(768|1024)' | cut -d '-' -f1 | sed -e 's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[6] grep -r "Trust (hostname)" *.log  | grep "does not match" | cut  -d '-' -f1 | sed -e 's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[7] grep -r "Chain of trust" *.log | grep "NOT ok" | grep "incomplete" | cut -d '-' -f1 | sed -e 's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[8] grep "SSLv2" *.log | grep "offered (NOT ok" | cut -d '-' -f1 | sed -e 's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[9] grep "SSLv3" *.log | grep "offered (NOT ok" | cut -d '-' -f1 | sed -e 's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[10] grep -w "TLS 1  " *.log | grep "offered" | cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[11] grep "BEAST" *.log | grep -v "no SSL3 or TLS1 (OK)" | cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[12] grep "SWEET32" *.log | grep "VULNERABLE" | cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[13] grep "LOGJAM" *.log | grep "VULNERABLE" | cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[14] grep "ROBOT" *.log | grep "VULNERABLE" | cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[15] grep "CRIME" *.log | grep "VULNERABLE" | cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[16] grep "RC4" *.log | grep "VULNERABLE" | cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[17] grep "Triple DES Ciphers" *.log | grep -vw "offered " | cut  -d '-' -f1 |sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[18] grep "CVE-2009-3555" *.log | grep "VULNERABLE" | cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[19] grep "Secure Client-Initiated Renegotiation" *.log | grep "VULNERABLE" |cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[20] grep "CVE-2013-3587" *.log | grep "NOT ok" |cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
[21] grep "CVE-2014-0224" *.log | grep "VULNERABLE" | cut  -d '-' -f1 | sed -e's/_p/:/g' -e "s/$/\/tcp/g" | uniq
```
