#!/usr/bin/env bash

function check_cve-2014-0224() {
	result=$(grep -H "CVE-2014-0224" *.log \
		| grep "VULNERABLE" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
		printf "\n# OpenSSL ChangeCipherSpec Vulnerability\n"
		printf "%s\n" ${result}
	fi
}

function check_cve-2013-3587() {
	result=$(grep -H "CVE-2013-3587" *.log \
		| grep "NOT ok" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
		printf "\n# HTTP Compression Used (Breach)\n"
		printf "%s\n" ${result}
	fi
}

function check_cve-2009-3555() {
	result=$(grep -H "CVE-2009-3555" *.log \
		| grep "VULNERABLE" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
		printf "\n# TLS Protocol Session Renegotiation Security (CVE-2009-3555)\n"
		printf "%s\n" ${result}
	fi
}


function check_client_renog() {
	result=$(grep -H "Secure Client-Initiated Renegotiation" *.log \
		| grep "VULNERABLE" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
		printf "\n# Denial of Service via Client-Initiated Renegotiation\n"
		printf "%s\n" ${result}
	fi
}

function check_3DES() {
	result=$(grep -H "Triple DES Ciphers" *.log \
		| grep -vw "offered " \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
		printf "\n# Triple DES Ciphers\n"
		printf "%s\n" ${result}
	fi
}

function check_rc4() {
	result=$(grep -H "RC4" *.log \
		| grep "VULNERABLE" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
		printf "\n# RC4 Ciphers\n"
		printf "%s\n" ${result}
	fi
}

function check_self_signed() {
	result=$(grep -H "Chain of trust" *.log \
		| grep "NOT ok" \
		| grep "self" \
		| cut  -d '-' -f1 \
		| sed -e 's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# Self-signed certificates\n" 
		printf "%s\n" ${result}
	fi
}

function check_cert_exp() {
	result=$(grep -H "Certificate Expiration" *.log \
		| grep "expired" \
		| cut  -d '-' -f1 \
		| sed -e 's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# Expired SSL/TLS certificates\n"
		printf "%s\n" ${result}
	fi
}

function check_wildcard() {
	result=$(grep -H "Common Name" *.log \
		| awk '{print $5,$1}' | egrep "\*" \
		| cut  -d '-' -f1 \
		| sed -e 's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq \
		| awk '{print $2,$1}')

	if [[ ${result} ]]; then
        	printf "\n# Wildcard certificate\n"
		printf "%s\n" ${result}
	fi
}

function check_sha1() {
	result=$(grep -H "Signature Algorithm" *.log \
		| grep "SHA1" \
		| cut  -d '-' -f1 \
		| sed -e 's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# Certificate signed with a weak hashing algorithm (SHA1)\n"
		printf "%s\n" ${result}
	fi
}

function check_key_size() {
	result=$(grep -H "Server key size" *.log \
		| grep "RSA" | grep -E '(768|1024)' \
		| cut -d '-' -f1 \
		| sed -e 's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# Certificate with a public key shorter than 2048 characters\n"
		printf "%s\n" ${result}
	fi
}

function check_fqdn() {
	result=$(grep -H "Trust (hostname)" *.log \
		| grep "does not match" \
		| cut  -d '-' -f1 \
		| sed -e 's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# Certificate's common name does not match the server's fully qualified domain name (FQDN)\n"
		printf "%s\n" ${result}
	fi
}

function check_chain_of_trust() {
	result=$(grep -H "Chain of trust" *.log \
		| grep "NOT ok" \
		| grep "incomplete" \
		| cut -d '-' -f1 \
		| sed -e 's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# Certificate signed by an unknown certificate authority (CA)\n"
		printf "%s\n" ${result}
	fi
}

function check_ssl2() {
	result=$(grep -H "SSLv2" *.log \
		| grep "offered (NOT ok" \
		| cut -d '-' -f1 \
		| sed -e 's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# SSLv2 supported\n"
		printf "%s\n" ${result}
	fi
}

function check_ssl3() {
	result=$(grep -H "SSLv3" *.log \
		| grep "offered (NOT ok" \
		| cut -d '-' -f1 \
		| sed -e 's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# SSLv3 supported\n"
		printf "%s\n" ${result}
	fi
}

function check_tls1() {
	result=$(grep -H -w "TLS 1  " *.log \
		| grep "offered" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)
	
	if [[ ${result} ]]; then
        	printf "\n# TLS1.0 supported\n"
		printf "%s\n" ${result}
	fi
}

function check_beast() {
	result=$(grep -H "BEAST" *.log \
		| grep -v "no SSL3 or TLS1 (OK)" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# BEAST\n"
		printf "%s\n" ${result}
	fi
}

function check_sweet32() {
	result=$(grep -H "SWEET32" *.log \
		| grep "VULNERABLE" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
        	printf "\n# SWEET32\n"
		printf "%s\n" ${result}
	fi
}

function check_logjam() {
	result=$(grep -H "LOGJAM" *.log \
		| grep "VULNERABLE" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)
	
	if [[ ${result} ]]; then
		printf "\n# LOGJAM\n"
		printf "%s\n" ${result}
	fi
}

function check_robot() {
	result=$(grep -H "ROBOT" *.log \
		| grep "VULNERABLE" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
		printf "\n# ROBOT\n"
		printf "%s\n" ${result}
	fi
}

function check_crime() {
	result=$(grep -H "CRIME" *.log \
		| grep "VULNERABLE" \
		| cut  -d '-' -f1 \
		| sed -e's/_p/:/g' -e "s/$/\/tcp/g" \
		| uniq)

	if [[ ${result} ]]; then
		printf "\n# CRIME\n"
		printf "%s\n" ${result}
	fi
}

check_cve-2014-0224 
check_cve-2013-3587 
check_cve-2009-3555 
check_client_renog 
check_3DES 
check_rc4 
check_self_signed 
check_cert_exp 
check_wildcard 
check_sha1 
check_key_size 
check_fqdn 
check_chain_of_trust 
check_ssl2 
check_ssl3 
check_tls1 
check_beast 
check_sweet32 
check_logjam 
check_robot 
check_crime 
